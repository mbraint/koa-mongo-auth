const dataBase = require('./dataBase');
const server = require('./server');


module.exports = {
  dataBase,
  server
}