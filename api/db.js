const mongoose = require('mongoose');
const db = mongoose.connection;


const connect = async (dbUrl, dbName) => {
  await mongoose.connect(dbUrl + dbName);
};


db.on('connected', () => {
  console.log(`Mongoose connection open to DB`);
});


db.on('error', (err) => {
  console.log('Mongoose connection error', err);
  process.exit(0);
});


db.on('disconnected', () => {
  console.log('Mongoose connection disconnected');
});


module.exports = {
  connect
};