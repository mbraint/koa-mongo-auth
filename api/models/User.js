const mongoose = require('mongoose');
const {hashPassword} = require('../helpers/crypto.js');


const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    default: ''
  },
  hashedPassword: {
    type: String,
    required: true,
    select: false
  },
  salt:{
    type: String,
    required: true,
    select: false
  }
})


// Register methods
userSchema.method({
  isValidPassword
})


/**
 * Check password for user
 * @param {String} password 
 */
function isValidPassword(password){
  const user = this;
  const hashedPassword = hashPassword(password, user.salt);

  return user.hashedPassword === hashedPassword
}


module.exports =  mongoose.model('User', userSchema)
