const KoaRouter = require('koa-router');
const authRouter = require('./auth');
const userRouter = require('./user');
const checkAuth = require('../middlewares/checkAuth');


const router = new KoaRouter({
  prefix: '/api/v1'
});


//Register middleware for routs
router.use(['/users'], checkAuth());
router.use(authRouter.routes());
router.use(userRouter.routes());


module.exports = router;