const KoaRouter = require('koa-router');
const passport = require('passport');
const User = require('./user');
const createResponse = require('../helpers/createResponse');


const router = new KoaRouter({
  prefix: '/auth'
})


/**
 * Authenticate.
 * NOTE: Do not change order for this 'router.use'!
 */
router.post('/signin', async (ctx) => {
  return passport.authenticate('local', (err, user, info, status) => {
    if (user === false) {
      ctx.body = createResponse(false);
      ctx.throw(401);
    } else {
      ctx.body = createResponse(true);
      return ctx.login(user)
    }
  })(ctx);
});


/**
 * Signout
 */
router.get('/signout', async (ctx, next) => {
  await ctx.logout();
  ctx.body = createResponse(true);
})



module.exports = router;