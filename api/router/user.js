const KoaRouter = require('koa-router');
const createResponse = require('../helpers/createResponse');
const UserController = require('../controllers/user');


const router = new KoaRouter({
  prefix: '/users'
})


/**
 * Get all users
 */
router.get('/', async (ctx, next) => {
  const users = await UserController.getAll();
  ctx.body = createResponse(true, users);
});


/**
 * Create new user
 */
router.post('/', async (ctx, next) => {
  const newUser = await UserController.createUser(ctx.request.body)
    .catch(err => {
      if(err.code === 11000){
        console.log('Error 11000');
        ctx.body = createResponse(false);
        next();
      }
    });
  
  if(newUser){
    ctx.res.statusCode = 201 // Created
    ctx.body = createResponse(true, newUser);
  }  
  
});


/**
 * Get user by username
 */
router.get('/:username', async (ctx, next) => {
  const user = await UserController.findByName(ctx.params.username);
  if (user) {
    ctx.body = createResponse(true, user);
  } else {
    ctx.body = createResponse(false);
    ctx.throw(404);
  }
});


module.exports = router;