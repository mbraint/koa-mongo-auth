const crypto = require('crypto');


/**
 * Generater random salt.
 * @returns {String} salt 
 */
function generateSalt(){
  return crypto.randomBytes(128).toString('base64');
}


/**
 * Hash password.
 * @param {String} password 
 * @param {String} salt
 * @returns {String} hashed password.
 */
function hashPassword(password, salt){
  return crypto.pbkdf2Sync(password, salt, 10000, 128, 'sha512').toString('base64');
}


module.exports = {
  generateSalt,
  hashPassword
}