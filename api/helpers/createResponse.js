
/**
 * Unificate all responses
 * @param {Boolean} success 
 * @param {*} data 
 */
function createResponse(success, data) {
  const resObj = {success};
  if(data) {
    resObj.data = data;
  }
  return resObj;
}

module.exports = createResponse