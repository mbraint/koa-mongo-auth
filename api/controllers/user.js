const User = require('../models/User.js');
const {generateSalt, hashPassword} = require('../helpers/crypto.js');


/**
 * Create new user
 * @param {Object} userData
 * @returns {Promise<>} 
 */
async function createUser(userData) {
  const {username, email, password} = userData;
  const salt = generateSalt();
  const hashedPassword = hashPassword(password, salt);

  return new User({
    username,
    email,
    hashedPassword,
    salt
  }).save()
}


/**
 * Find user by name
 * @param {String} username
 * @param {Boolean} isAuthReg 
 * @returns {Promise<User>} finded user
 */
async function findByName(username, isAuthReq = false){
  const select  = isAuthReq ? '+hashedPassword +salt' : ''
  return User.findOne({username}).select(select).exec();
}


/**
 * Find user by ID
 * @param {String} id
 * @returns {Promise<User>} finded user 
 */
async function findById(id){
  return User.findById(id).exec();
}


/**
 * Get all users
 * @returns {Promise<Array<User>>} users 
 */
async function getAll(){
  return User.find().exec()
}


module.exports = {
  createUser,
  findByName,
  findById,
  getAll
}