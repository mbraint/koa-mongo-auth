const createResponse = require('../helpers/createResponse');


/** 
 * Check auth. 
 */
function checkAuth(){
  return async (ctx, next) => {
    if (ctx.isAuthenticated()) { return next(); }
    else {
      ctx.body = createResponse(true)
      ctx.throw(401)
    }
  }
}

module.exports = checkAuth;