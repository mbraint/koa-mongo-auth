const passport = require('passport');
const LocalStratagy = require('passport-local').Strategy;
const UserController = require('./controllers/user');


passport.serializeUser((user, done) => {
  done(null, user.id)
})


passport.deserializeUser(async (id, done) => {
  try {
    const user = await UserController.findById(id)
    done(null, user)
  } catch(err) {
    done(err)
  }
})


const localStrategy = new LocalStratagy(async (username, password, done) => {
  const user = await UserController.findByName(username, true).catch(err => done(err));
  if(!user) {
    return done(null, false, {message: 'Incorrect username.'})
  };

  if(!user.isValidPassword(password)){
    return done(null, false, {message: 'Incorrect password.'})
  }

  return done(null, user);

});



module.exports = localStrategy;