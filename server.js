const Koa = require('koa');
const logger = require('koa-logger');
const bodyParser = require('koa-bodyparser');
const passport = require('koa-passport');
const session = require('koa-session');
const localStratagy = require('./api/auth');
const config = require('./config');
const db = require('./api/db');
const router = require('./api/router/router');

const app = new Koa();

app.keys = ['super-secret-key'];
app.use(session(app));

//Passport
passport.use(localStratagy);
app.use(logger());
app.use(bodyParser());
app.use(passport.initialize());
app.use(passport.session())


// Api router
app.use(router.routes());
app.use(router.allowedMethods());


// Startup
db.connect(config.dataBase.url, config.dataBase.name)
app.listen(config.server.port, config.server.hostName);